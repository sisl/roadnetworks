Road Networks
============

This library allows for the parsing of Road Network Definition Files (RNDFs), as used in the autonomous driving community and originally used by DARPA in the Grand Challenge.

Please contact Tim Wheeler with any questions.



